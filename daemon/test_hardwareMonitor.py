from unittest import TestCase
from HardwareMonitor import HardwareMonitor


class TestHardwareMonitor(TestCase):
    def test_getRamUsageInPercent(self):
        monitor = HardwareMonitor()
        expected_usage = 83.304991
        self.assertAlmostEqual(monitor.getRamUsageInPercent(command="cat FilesUsedInTests/memoryInfo | grep Mem"),
                               expected_usage)

    def test_getCpuUsageInPercent(self):
        monitor = HardwareMonitor()
        expected_usage = 34.3
        self.assertAlmostEqual(monitor.getCpuUsageInPercent(command="cat FilesUsedInTests/topInfo | grep Cpu"),
                               expected_usage)

    def test_getNetworkSpeed(self):
        monitor = HardwareMonitor()
        expected_download_speed = 7848
        expected_upload_speed = 2268
        self.assertTupleEqual(monitor.getNetworkSpeed(command1="cat FilesUsedInTests/networkInfo1 | grep %s",
                                                      command2="cat FilesUsedInTests/networkInfo2 | grep %s"),
                              (expected_download_speed, expected_upload_speed))


if __name__ == '__main__':
    TestCase.main()

#!/usr/bin/env python
import logging
import sys
import os
import time
from optparse import OptionParser
from daemon import Daemon

"""HardwareMonitor.py: Monitor and show usage of cpu, memory and network

__author__ = "Ryszard Mikulec" """


class HardwareMonitor(Daemon):
    def __init__(self, pidfile='/tmp/pidfile.pid', logfile='logfile.log'):
        """Init object with path to file with pid and file with logs"""
        Daemon.__init__(self, pidfile)
        self.logfile = logfile

    def parse_options(self):
        """Parse options from argv"""
        self.parser = OptionParser(usage="usage: %prog [options]")
        self.add_options()
        (self.options, self.args) = self.parser.parse_args()

    def add_options(self):
        """Add option to parser"""
        self.parser.add_option("--start", action="store_true", dest="startDeamon", default=True,
                               help="run deamon [default]")
        self.parser.add_option("--stop", action="store_false", dest="startDeamon", help="stop deamon")
        self.parser.add_option("-a", "--all", action="store_true", dest="showAll", default=True,
                               help="show usage of cpu, memory and network [default]")
        self.parser.add_option("-c", "--cpu", action="store_true", dest="showCPU", default=False,
                               help="show usage of cpu")
        self.parser.add_option("-m", "--memory", action="store_true", dest="showMemory", default=False,
                               help="show usage of memory")
        self.parser.add_option("-n", "--network", action="store_true", dest="showNetwork", default=False,
                               help="show usage of network")
        self.parser.add_option("-t", "--time", action="store", type="int", dest="interval", default=3,
                               help="set interval of time in seconds [default = 3]")
        self.parser.add_option("--clear", action="store_true", dest="clearLogs", default=False,
                               help="Clear Logs [default=False]")

    def handleOptions(self):
        self.setOptions()
        if len(sys.argv) == 1:
            self.parser.print_help()
        elif self.options.startDeamon:
            logging.info("start running")
            print("Daemon is runing ...")
            self.start()
        else:
            logging.info("stop running")
            print("Daemon is dead")
            self.stop()

    def setOptions(self):
        mode = "w" if self.options.clearLogs  else "a"
        logging.basicConfig(filename=self.logfile, filemode=mode, level=logging.DEBUG, format='%(asctime)s %(message)s')
        if self.options.showCPU or self.options.showMemory or self.options.showNetwork:
            self.options.showAll = False
        else:
            self.options.showCPU = self.options.showMemory = self.options.showNetwork = True

    def run(self):
        """Overriden method from daemon.py"""
        while True:
            message = ""
            if self.options.showCPU:
                message = message + "\nusage of CPU: " + str(self.getCpuUsageInPercent()) + "%"
            if self.options.showMemory:
                message = message + "\nusage of RAM: " + ('%.1f' % self.getRamUsageInPercent()) + "%"
            if self.options.showNetwork:
                speed = self.getNetworkSpeed()
                message = message + "\nCurrent download speed: " + str(
                    speed[0]) + "Bps " + "Current upload speed: " + str(speed[1]) + "Bps"
            logging.info(message)
            time.sleep(self.options.interval)

    def getRamUsageInPercent(self, command="free | grep Mem"):
        line = os.popen(command).readline()
        words = line.split()
        totalRam = float(words[1])
        usedRam = float(words[2])
        usedRamInPercents = usedRam / totalRam * 100
        return usedRamInPercents

    def getCpuUsageInPercent(self, command="top -b -n 1 | grep Cpu"):
        line = os.popen(command).readline()
        freeCpu = float(line.split()[7])
        usedCpuInPercent = 100.0 - freeCpu
        return usedCpuInPercent

    def getNetworkSpeed(self, command1="cat /proc/net/dev | grep %s", command2="cat /proc/net/dev | grep %s"):
        """return tuple, where first element is speed download,
        second element is speed upload in Byte per second"""
        line1 = os.popen(command1 % "eth0").readline()
        line2 = os.popen(command1 % "wlan0").readline()
        downloadSpeed = 0 - int(line1.split()[1]) - int(line2.split()[1])
        uploadSpeed = 0 - int(line1.split()[9]) - int(line2.split()[9])
        time.sleep(1)
        line1 = os.popen(command2 % "eth0").readline()
        line2 = os.popen(command2 % "wlan0").readline()
        downloadSpeed += int(line1.split()[1]) + int(line2.split()[1])
        uploadSpeed += int(line1.split()[9]) + int(line2.split()[9])
        return downloadSpeed, uploadSpeed


if __name__ == "__main__":
    program = HardwareMonitor()
    program.parse_options()
    program.handleOptions()
